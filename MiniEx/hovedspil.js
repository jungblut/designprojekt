var baggrund;

// Variabler for app
var knapprofil;
var profil;
var knapmenu;
var menu;
var knapg;
var knapv;
var play;
var pause;
//var ruter;

// variabler for sprites
var hjerte;
//var figur;

function preload(){

  //Grafik til app preloades
  baggrund = loadImage("grafik/baggrund.jpg");
  profil = loadImage('grafik/profil.png');
  menu = loadImage('grafik/menu.png');
  ruter = loadImage('grafik/ruter.png');
  gaa = loadImage('grafik/gaa.png');
  play = loadImage('grafik/play.png');
  pause = loadImage('grafik/pause.png');

  //Grafik til sprite preloades
  hjerte = loadAnimation('sprite/hjerte0001.png' , 'sprite/hjerte0009.png');
  //figur = loadAnimation('sprite/figur0001.png' , 'sprite/figur0005.png');
}


function setup(){

  //canvas og baggrundsbillede
  createCanvas(1380, 680);
  image(baggrund, 0, 0);

  //Profil-, menu- og "gemteruter"knap oprettes, så de er på startskærm
  knapprofil = createImg('grafik/knapprofil.png');
  knapprofil.position(173 , 37);
  knapprofil.mousePressed(showProfil);

  knapmenu = createImg('grafik/knapmenu.png');
  knapmenu.position(40 , 42);
  knapmenu.mousePressed(showMenu);

  knapg = createImg('grafik/knapg.png');
  knapg.position(55 , 256);
  knapg.mousePressed(showRuter);

}

//funktion for menuknappen
function showMenu() {
  // Nyt app billede loades
  image(menu, 35.5, 66);
  //Ny knap "gemte ruter" oprettes
  knapg = createImg('grafik/knapg.png');
  knapg.position(55 , 256);
  knapg.mousePressed(showRuter);
  //andre ikke relevante knapper gemmes
  knapv.hide();
}

//funktionerne showProfil/-Ruter/-Gaa/-Pause fungerer på samme måde som showMenu
function showProfil() {
  image(profil, 35.5, 66);

  knapg.hide();
  knapv.hide();
  pause.hide();
  play.hide();
}

function showRuter() {
  image(ruter, 35.5, 66);

  knapv = createImg('grafik/knapv.png');
  knapv.position(55, 206);
  knapv.mousePressed(showGaa);

  knapg.hide();
  pause.hide();
  play.hide();
}

function showGaa() {
  image(gaa, 35.5, 66);

  play = createImg('grafik/play.png');
  play.position(75, 265);
  play.mousePressed(showPause);

  knapg.hide();
  knapv.hide();
}

function showPause(){
  pause = createImg('grafik/pause.png');
  pause.position(75, 265);
  pause.mousePressed(hidePause);
}

//Når der trykkes på pause knappen, gemmes den og playknappen kommer frem igen
function hidePause(){
  pause.hide();
}


function draw() {
  fill(255);
  noStroke();
  rect(50, 480, 150, 130);
  animation(hjerte, 118, 550);
  //animation(figur, 400, 500);
}
